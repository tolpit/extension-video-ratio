let components = [];
let tree       = [];

const observer = new MutationObserver(mutations => {
  mutations.forEach(function(mutation) {
    mutation.addedNodes.forEach(node => {
      if(node.nodeName === "#text" || node.nodeName === "#comment") return;

      components.forEach(component => {
        const selector = component.selector();

        try {
          if(node.querySelector(selector) || (document.body.querySelector(selector) && document.body.querySelector(selector).isSameNode(node))) {
            const element = node.querySelector(selector) || document.body.querySelector(selector);

            tree.push(
              new component(element)
            );
          }
        } catch(err) {}

      });
    })
  });
});

const observerConfig = {
  attributes: false,
  childList: true,
  characterData: false,
  subtree: true
};

observer.observe(document.body, observerConfig);

export {components,observer,tree};