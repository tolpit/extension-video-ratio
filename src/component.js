import {components} from "./app";

export default class Component {

  constructor(element) {
    this.element = element;
  }

  static append(componentDeclaration) {
    components.push(componentDeclaration);
  }

}