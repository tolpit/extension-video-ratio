import VideoComponent from '../video.component.js'

export default class DefaultVideoComponent extends VideoComponent {

  static selector() { return "video"; }

}

VideoComponent.append(DefaultVideoComponent);