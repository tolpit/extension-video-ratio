import VideoComponent from '../video.component.js'

export default class NetflixVideoComponent extends VideoComponent {

  static selector() { return ".video-container video" };

}

VideoComponent.append(NetflixVideoComponent);