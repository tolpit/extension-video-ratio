import Component from '../component.js'

export default class VideoComponent extends Component {

  constructor(element) {
    super(element);

    // cover on 21:9 screen ratio, contain on the others
    this.ratio = (window.screen.availWidth / window.screen.availHeight) > 2 ? "cover" : "contain";

    this.applyRatioToVideo();
  }

  applyRatioToVideo() {
    this.element.style.objectFit      = this.ratio;
    this.element.style.objectPosition = "center";
  }

}