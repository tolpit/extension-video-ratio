import {components,observer} from "./app"

import DefaultVideoComponent from './components/default/DefaultVideo.js'
import NetflixVideoComponent from './components/netflix/NetflixVideo.js'

// 1. Detect the platform
// 2. Observe for DOM modifications
// 3. When the video is detected,
//   3.2 Launch the handler for the video,
//   3.1 Based on the user choice (or default) apply a ratio to feet the video